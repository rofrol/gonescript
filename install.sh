#!/bin/bash
######### Bash specific ######################################################
#run 19th line:
#sh install.sh 19
if [ $1 -gt 0 ];then
 p=`eval "sed '${1}q;d' install.sh"`
 read -p "$p"
 eval "$p"
fi
exit
#More info
#http://wooledge.org:8000/BashFAQ
#http://www.unixguide.net/unix/sedoneliner.shtml
#without eval redirects > won't run.

#or add in $HOME/.vimrc and use F3, to see output again use :!
#function Eval_line( )
#  :let start=line( '.' )
#   :exe "!eval" getline(start)
#endfunction
#map <F3> :call Eval_line()

#In case of system freeze
#alt+sysrq+d check if works
#alt+sysrq+r,e,i,s,u,b
links http://en.wikipedia.org/wiki/Magic_SysRq_key

######### Cheat sheet ########################################################
#fdisk mkfs.* mount bind stage portage kernel grub fstab passwd
links http://en.gentoo-wiki.com/wiki/Wireless_Networking
#wireless-tools wpa_supplicant dhcpcd
#ntfs3g pciutils (include lspci)
#vim links gpm git (USE=tk to get gitk) htop gentoolkit (equery and others)
links http://www.gentoo.org/doc/en/alsa-guide.xml
#alsa-utils alsaconf mpg123
#gpasswd -a <username> audio
links http://www.gentoo.org/doc/en/xorg-config.xml
#xorg-x11 USE+="X hal dbus" INPUT_DEVICES="evdev" VIDEO_CARDS="intel"
links http://bugs.gentoo.org/251093
#touchpad sensitivity problem, maybe to high
links http://www.gentoo.org/doc/en/gnome-config.xml
#gnome USE+="gtk gnome cairo" xdm

#gdm (to have shutdown and reboot), /etc/X11/gdm/custom
#run gdmsetup (set GtkRC file), gdmphotosetup (~/.face, MaxIcon* in custom)
#gdmthemetester xdmcp circles, /usr/share/gdm/themes

#x11-libs/gtk+ jpeg #to have wallpapers and gdm themes with jpeg
#MAKEOPTS="-j1" emerge -1 gnome-user-docs && emerge gnome
links http://bugs.gentoo.org/260827
links http://bugs.gentoo.org/150909
links http://wiki.archlinux.org/index.php/Xorg_input_hotplugging
echo "eval `dbus-launch --sh-syntax --exit-with-session`" > ~/.xinitrc
echo "exec gnome-session" >> ~/.xinitrc

gconftool-2 --set "/apps/panel/objects/object_0/use_custom_icon" --type boolean "true"
gconftool-2 --set "/apps/panel/objects/object_0/custom_icon" --type string "/mnt/sda3/gentoo/packets/start_gentoo.png"

#mplayer (USE=X is required to run xv?)
#xclip

#unetbootin downloaded from web, requires mtools and 7z
mtools
p7zip

#emerge -vat --jobs=2 gnome --color y | less


#startx warnings
#type1 (maybe install  media-fonts/font-xfree86-type1)
#freetype
#session manager
#and others

#localization

#better backup from
links http://wiki.laptop.org/go/Backup-and-Restore-Using-SysRescueCD

#energy consumption, hibernation etc.

#useradd

#network manager?

#touchpad sensitivity and tap to click, disable touchpad when typing, scrolling

#autologgin gdm?

#pulseaudio (lags?)

######### Handbook ###########################################################
links www.gentoo.org/doc/handbook

umount /mnt/gentoo

#prepare disk with fdisk
#add -c to check blocks
mke2fs -j -L gentoo /dev/sda2
mkdir /mnt/gentoo
mount -L gentoo /mnt/gentoo

#wget -c "weekly stages and portage from gentoo.org"
time tar -vpxjf /mnt/storage/projects/gentoo/packets/stage3-i686-20090401.tar.bz2 -C /mnt/gentoo
time tar -vpxjf /mnt/storage/projects/gentoo/packets/portage-20090414.tar.bz2 -C /mnt/gentoo/usr

#edit /mnt/gentoo/etc/fstab

mount -o bind /dev /mnt/gentoo/dev/
mount -t proc none /mnt/gentoo/proc/

mkdir /mnt/gentoo/mnt/storage
mount -o bind /mnt/storage /mnt/gentoo/mnt/storage

cp /etc/resolv.conf /mnt/gentoo/etc/resolv.conf

#emerge grub && edit /boot/grub/grub.conf
emerge genkernel gentoo-sources
genkernel --menuconfig --kernel-config=$HOME/genkernel.config --disklabel all

#or use existing kernel
cp /mnt/storage/sandbox/boot/*-gentoo-r8 /mnt/gentoo/boot
mkdir /mnt/gentoo/lib/modules -p
cp -R /mnt/storage/sandbox/lib/modules/*-gentoo-r8/ /mnt/gentoo/lib/modules
cp -R /mnt/storage/sandbox/lib/firmware /mnt/gentoo/lib

chroot /mnt/gentoo /bin/bash
env-update
source /etc/profile
#doesn't work for me
export PS1="(chroot) $PS1"

emerge --sync

passwd

######### Backup #############################################################
mkdir /mnt/sda3
ntfs-3g /dev/sda3 /mnt/sda3
mkdir /mnt/gentoo
mount -L gentoo /mnt/gentoo
cd /mnt/gentoo
time tar -vpxjf /mnt/sda3/gentoo/backup/gentoo-20090416.tar.bz2 -C /mnt/gentoo
#~5min
time tar -vpcjf /mnt/sda3/gentoo/backup/gentoo-20090416.tar.bz2 .
#~20min

#backup should be done on another disk
#backup bootsector (with grub)
dd if=/dev/sda of=/mnt/sda3/gentoo/backup/bootsector.sda.dd bs=446 count=1
#backup partition table
sfdisk -d /dev/sda > /mnt/sda3/gentoo/backup/table.sda.sfdisk

#restore bootsector
dd if=/mnt/sda3/gentoo/backup/bootsector.sda.dd of=/dev/sda bs=446 count=1

#or with grub
#chroot /mnt/gentoo /bin/bash
#grub
#grub> device (hd0) /dev/sda
#grub> root (hd0,1)
#grub> setup (hd0)
#grub> quit

#restore partition table
sfdisk /dev/sda < /mnt/sda3/gentoo/backup/table.sda.sfdisk

#clone partition table
sfdisk -d /dev/sda | sfdisk /dev/sdb

emerge -vat media-fonts/liberation-fonts
#To substitute Liberation fonts for Microsoft equivalents, use:
eselect fontconfig enable 60-liberation.conf

#lokalizacja
ln -sf /usr/share/zoneinfo/right/Europe/Warsaw /etc/localtime
vim /etc/conf.d/clock
vim /etc/env.d/02locale
vim /etc/locale.gen
locale-gen
vim /etc/conf.d/keymaps
vim /etc/conf.d/consolefont

#totem wmv and other codecs
#differencies between good, ugly and bad
links http://cgit.freedesktop.org/gstreamer/gst-plugins-good/tree/README
emerge -vat gst-plugins-ugly
#x264
echo "media-plugins/gst-plugins-x264 ~x86" >> /etc/portage/package.keywords
echo "media-libs/gst-plugins-base ~x86" >> /etc/portage/package.keywords
echo "media-libs/gstreamer ~x86" >> /etc/portage/package.keywords
emerge -vatb gst-plugins-x264
#flash flv
#error gst-plugins-base ~x86 so gst-plugins-bad also
echo "media-libs/gst-plugins-bad ~x86" >> /etc/portage/package.keywords
emerge -vatb gst-plugins-bad


#apache php
echo "APACHE2_MPMS=\"prefork\"" >> /etc/make.conf
emerge -vatb apache

#wymagane flagi dla apache: cgi force-cgi-redirect
echo "=dev-lang/php-5* cgi force-cgi-redirect mysql mysqli apache2" >> /etc/portage/package.use
emerge -vatb =dev-lang/php-5*
#add '-D PHP5' to APACHE2_OPTS
vim /etc/conf.d/apache2

echo "www-apache/mod_suphp mode-force" >> /etc/portage/package.use
echo "www-apache/mod_suphp ~x86" >> ${mnt_point}/etc/portage/package.keywords
emerge -vatb www-apache/mod_suphp
#add '-D SUPHP' to APACHE2_OPTS
vim /etc/conf.d/apache2

#in .htacces you can control if apache should treat php files as php4 or php5
#AddHandler x-httpd-php4 .php

/etc/conf.d/apache2 start

#mysql won't let configure itself, when hostname is localhost.
vim /etc/conf.d/hostname
#HOSTNAME="m1330"
/etc/init.d/hostname restart

emerge -vatb --config mysql
/etc/conf.d/mysql start

#but if you change hostname you'll get this
#apache2: apr_sockaddr_info_get() failed for dell_stacjonarny
#have to add hostname to the end of 127.0.0.1 line in /etc/hosts
#but if you see this
#apache2: Could not reliably determine the server's fully qualified domain name, using 127.0.0.1 for ServerName
#you have to add localhost.localdomain in line 127.0.0.1
vim /etc/hosts
#127.0.0.1       localhost.localdomain localhost m1330

#look at errors here
vim /var/log/apache2/error_log
